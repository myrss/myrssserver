﻿using System.Web;
using System.Web.Optimization;

namespace MyRssServer
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/Content/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));
            bundles.Add(new StyleBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bower_components/bootstrap/dist/css/bootstrap.min.css",
                      "~/Content/font-awesome.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/Scripts/bower").Include(
                "~/Scripts/bower_components/angular-local-storage/dist/angular-local-storage.js",
                "~/Scripts/bower_components/angular-bootstrap/ui-bootstrap.js",
                "~/Scripts/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
                "~/Scripts/bower_components/angular-ui-router/release/angular-ui-router.js"
                ));

            bundles.Add(new Bundle("~/Scripts/RSSapp").Include(
                      "~/Scripts/app/app.js",
                      "~/Scripts/app/controllers/categories.js",
                      "~/Scripts/app/controllers/channels.js",
                      "~/Scripts/app/controllers/main.js",
                      "~/Scripts/app/controllers/user.js",
                      "~/Scripts/app/directives/ellipsis.js",
                      "~/Scripts/app/models/category.js",
                      "~/Scripts/app/models/channel.js",
                      "~/Scripts/app/models/item.js",
                      "~/Scripts/app/services/auth.js",
                      "~/Scripts/app/services/authInterceptor.js",
                      "~/Scripts/app/services/rssService.js"
                      ));
        }
    }
}
