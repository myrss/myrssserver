﻿app.factory('rssService', ['$q', '$http', function ($q, $http) {
    var _categories = { fetch: false, list: {} }
        , _channels = { list: {} }
        , _items = { list: {} }
    ;

    function loadCategories(callback) {
        $http.get("/api/Categories/").then(function (response) {
            _categories.list = {};
            for (var i = 0, len = response.data.length; i < len; ++i) {
                var c = response.data[i];
                console.log('Cat', c.Title);
                _categories.list[c.Id] = new Category({
                    id: c.Id, title: c.Title
                });
            }
        });
    }

    function loadCategory(categoryId, callback) {
        console.log("Loading category [%s]", categoryId);
        $http.get("/api/Categories/" + categoryId)
            .success(function (response) {
                var cat = response,
                    newCat = new Category({
                        id: cat.Id,
                        title: cat.Title
                    });
                console.log("Category received", newCat);
                _categories.list[cat.Id] = newCat;
                callback(null, newCat);
            })
            .error(function (err, status) {
                console.error('Could not get category', categoryId, err, status);
                callback(err, status)
            });

    }

    function loadChannel(channelId, callback) {
        $http.get("/api/Channels/" + channelId)
            .success(function (response) {
                var channel = response,
                    newChannel = new Channel({
                        id: channel.Id,
                        description: channel.Description,
                        link: channel.Link,
                        title: channel.Title
                    });
                _channels[channel.Id] = newChannel;
                callback(null, newChannel);
            })
            .error(function (err, status) {
                console.error('Could not get channel', channelId, err, status);
                callback(err, status)
            });

    }

    return {
        addChannel: function (channel, callback) {
            $http.post("/api/Channels/", JSON.stringify({ channel: { link: channel.link, title: 'test' }, categories: channel.categories }))
                .success(function (response) { callback(null, response) })
                .error(callback);
        },
        createCategory: function (category, callback) {
            $http.post("/api/Categories/", category.title)
                .success(function (response) { callback(null, response) })
                .error(callback);
        },
        editChannel: function (channel, callback) {
            $http.put("/api/Channels/" + channel.id + "/", { id:channel.id, link: channel.link })
                .success(function (response) {
                    console.log("PUT Channel", response);
                    callback(null, response)
                })
                .error(callback);
        },
        getCategories: function () {
            if (_categories.fetch == false) {
                //console.log();
                loadCategories(function (err, response) {
                    if (!err) {
                        console.log("Categories received:", response);
                        return _categories;
                    } else {
                        console.error("Could not get categories.");
                        return null;
                    }
                });
            }
            return _categories;
        },
        getCategory: function (categoryId, callback) {
            if (_categories.list[categoryId] == undefined) {
                loadCategory(categoryId, function (err, response) {
                    callback(null, response)
                });
            } else {
                callback(null, _categories.list[categoryId]);
            }
        },
        getChannel: function (channelId, callback) {
            if (_channels.list[channelId] != undefined) {
                callback(null, _channels.list[channelId]);
            } else {
                loadChannel(channelId, callback);
            }
        },
        getChannelsFromCategory: function (cat, callback) {
            var channels = [];
            console.log("Getting channels from [%s]...", cat.title);
            //if (_channels.contains()) {
            // Check if channels already fetched
            $http.get('/api/Categories/' + cat.id + '/channels')
                .success(function (response) {
                    console.log('Channels', response);
                    for (var i = 0, len = response.length; i < len; ++i) {
                        var channel = response[i],
                         newChannel = new Channel({
                             id: channel.Id,
                             description: channel.Description,
                             link: channel.Link,
                             title: channel.Title
                         });
                        cat.channels.push(channel.Id);
                        _channels.list[channel.Id] = newChannel;
                        channels.push(newChannel);
                        callback(null, channels);
                    }
                }).error(function (err, status) {
                    console.error('Could not get channels', err, status);
                    callback(err, null)
                });
            //        } else {

            //}
        },
        getItemsFromCategory: function (category, callback) {
            var items = [];
            for (var c in category.channelsObjects) {
                items.concat(c.itemsObjects);
            }
            callback(null, items);
            //$http.get('/api/Channels/' + channelId + '/items')
            //    .success(function (response) {
            //        var channel = _channels[channelId];
            //        for (var i = 0, len = response.length; i < len; ++i) {
            //            var item = response[i],
            //                newItem = new Item({
            //                    id: item.Id,
            //                    description: item.Description,
            //                    link: item.Link,
            //                    title: item.Title
            //                });
            //            if (channel != undefined) {
            //                channel.items.push(item.Id);
            //                channel.itemsObjects.push(newItem);
            //            }
            //            _items.list[item.Id] = newItem;
            //            items.push(newItem);
            //        }
            //        callback(null, items);
            //    }).error(function (err, status) {
            //        console.error('Could not get channels', err);
            //        callback(err, status);
            //    });
        }, getItemsFromChannel: function (channelId, callback) {
            var items = [];
            $http.get('/api/Channels/' + channelId + '/items')
                .success(function (response) {
                    var channel = _channels[channelId];
                    for (var i = 0, len = response.length; i < len; ++i) {
                        var item = response[i],
                            newItem = new Item({
                                id: item.Id,
                                description: item.Description,
                                link: item.Link,
                                read: item.Read,
                                title: item.Title
                            });
                        if (channel != undefined) {
                            channel.items.push(item.Id);
                            channel.itemsObjects.push(newItem);
                        }
                        _items.list[item.Id] = newItem;
                        items.push(newItem);
                    }
                    callback(null, items);
                }).error(function (err, status) {
                    console.error('Could not get channels', err);
                    callback(err, status);
                });
        },
        loadCategories: loadCategories,
        refreshChannel: function (channelId, callback) {
            $http.get('/api/Channels/' + channelId + '/refresh')
                .success(function (response) {
                    callback(null);
                }).error(function (err, status) {
                    console.error('Could not update channels', err);
                    callback(err, status);
                });
        },
        clear: function () {
            _categories.fetch = false;
            _categories.list = {};
            _channels.list = {};
        }
    }
}]);