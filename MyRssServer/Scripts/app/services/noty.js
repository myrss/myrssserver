﻿/**
 * Created by Matthias Brosolo on 04/12/2014.
 */
//var noty = angular.module('notyService');

var noty_default = {
    layout: 'topRight',
    timeout: '3000',
    maxVisible: 3
};

app.factory('noty', function () {

    return {
        info: function (text) {
            var params = angular.extend({}, noty_default, { type: 'info', text: text });
            noty(params);
        },
        error: function (text) {
            var params = angular.extend({}, noty_default, { type: 'alert', text: text });
            noty(params);
        },
        success: function (text) {
            var params = angular.extend({}, noty_default, { type: 'success', text: text });
            noty(params);
        }
    };
});