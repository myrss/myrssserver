﻿function Category(params) {
    this.title = "";
    this.channels = [];
    this.opened = undefined;

    angular.extend(this, params);
}