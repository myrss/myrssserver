﻿function Item(params) {
    this.title = "";
    this.link = "";
    this.description = "";
    this.read = false;

    this.language = "";
    this.copyright = "";
    this.managingEditor = "";
    this.webMaster = "";
    this.pubDate = "";
    this.lastBuildDate = "";
    this.category = "";
    this.generator = "";
    this.docs = "";
    this.cloud = "";
    this.ttl = "";
    this.image = "";
    this.rating = "";
    this.textInput = "";
    this.skipHours = "";
    this.skipDays = "";

    angular.extend(this, params);
}