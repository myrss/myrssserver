﻿function Channel(params) {
    this.title = "";
    this.link = "";
    this.description = "";
    this.items = [];
    this.itemsObjects = [];


    this.language = "";
    this.copyright = "";
    this.managingEditor = "";
    this.webMaster = "";
    this.pubDate = "";
    this.lastBuildDate = "";
    this.category = "";
    this.generator = "";
    this.docs = "";
    this.cloud = "";
    this.ttl = "";
    this.image = "";
    this.rating = "";
    this.textInput = "";
    this.skipHours = "";
    this.skipDays = "";

    angular.extend(this, params);
}

Channel.prototype.getDTO = function () {
    return {
        description: this.description,
        link:this.link,
        title:this.title,
    }
};