﻿//var module = angular.module('ellipsis');

app.directive('myEllipsis', ['$timeout', function ($timeout) {
    function link(scope, element) {
        $timeout(function () {
            console.log('Ellipsis');
            element.ellipsis();

        }, 0);
    }

    return {
        link: link,
        replace: true,
        restrict: 'AE',
        scope: {}
    };
}]);