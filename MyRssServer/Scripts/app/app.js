﻿var app = angular.module("RSSApp", [
    //'ellipsis',
    //"angular-route"
    'LocalStorageModule',
    'ui.bootstrap',
    'ui.router'
]);

app.config(function ($httpProvider, $stateProvider, $urlRouterProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
    $urlRouterProvider.otherwise("/");
    $stateProvider
      //.state('main', {
      //    url: "/",
      //    templateUrl: "Templates/Login",
      //    controller:''
      //})
      .state('home', {
          url: "/",
          templateUrl: "Templates/Home",
          controller: function ($scope) {

          }
      })
      .state('all', {
          url: "/all",
          templateUrl: "Templates/Channel",
          controller: function ($scope) { }
      })
      .state('login', {
          url: "/login",
          templateUrl: "Templates/Login",
          controller: function ($scope, authService, $state, $location) {
              $scope.credentials = { userName: '', password: '' };

              $scope.login = function (credentials) {
                  authService.login(credentials)
                      .then(function (response) {
                          $scope.failure = false;
                          console.log('Authentified');
                          //$location.path('/');
                         $scope.$emit('authentified');
                         $state.go("home");
                         window.location.reload();
                         //$state.reload();
                      }, function (err) {
                          console.error('Login failed', err);
                          //noty({text:"fail"});
                          //$scope.message = err.error_description;
                      });
              };
          }
      })
      .state('logout', {
          url: "/logout",
          //templateUrl: "Templates/Login",
          template: "<div ui-view></div>",
          controller: function ($scope, $state, authService, $location) {
              authService.logOut();
              $scope.$emit('unauthentified');
              $state.go("home");
              window.location.reload();
          }
      })
      .state('categories', {
          url: "/categories",
          template: "<div ui-view></div>"
      })
        .state('categories.create', {
            url: "/create",
            templateUrl: "Templates/CategoryCreate",
            controller: function ($scope, rssService) {
                $scope.category = new Category();
                $scope.create = function (category) {
                    rssService.createCategory(category, function (err, response) {
                        if (err) {
                            console.error("Could not create category", err, response);
                        } else {
                            console.log("Category created", response);
                        }
                    });
                };
            }
        })
      .state('categories.items', {
          url: "/categories/:catId/items",
          templateUrl: "Templates/Category",
          controller: categoryItems
      })
      .state('channels', {
          url: "/channels",
          template: "<div ui-view></div>"
      })
      .state('channels.add', {
          url: "/add",
          templateUrl: "Templates/ChannelCreate",
          controller: channelAdd

      })
    .state('channels.items', {
        url: "/:channelId/items",
        templateUrl: "Templates/Channel",
        controller: channelItems
    })
    .state('channels.edit', {
        url: "/:channelId/edit",
        templateUrl: "Templates/ChannelCreate",
        controller: channelEdit
    })
    .state('items', {
        url: "/items/:itemsId",
        templateUrl: "Templates/ItemDetails",
        controller: channelEdit
    })
    ;
});