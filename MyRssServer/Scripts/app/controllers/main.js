﻿var app = angular.module("RSSApp");

app.controller('mainCtrl', function ($scope, $http, $modal, authService, $state, rssService) {
    $scope.categories = undefined;
    $scope.channels = {};
    $scope.items = {};
    $scope.sidebar = true;
    $scope.isAuth = authService.authentication.isAuth;

    $scope.templates = { channel: '/templates/channel' };

    $scope.current = {
        template: $scope.templates.channel,
        //template: 'default_view',
        data: {}
    };

    $scope.$on('authentified', $scope.authentified);
    $scope.authentified = function () {
        $scope.categories = rssService.getCategories();
        console.log("categories", $scope.categories);
        $scope.isAuth = authService.authentication.isAuth;
    };
    $scope.$on('unauthentified', $scope.unauthentified);

    $scope.unauthentified = function () {
        $scope.isAuth = authService.authentication.isAuth;
        rssService.clear();
    };
    //$scope.getCategories = function () {
    //    $http.get('/api/Categories/').success(function (response) {
    //        console.log('Categories', response);
    //        $scope.categories = {};
    //        for (var i = 0, len = response.length; i < len; ++i) {
    //            console.log('Cat', response[i].Title);
    //            $scope.categories[response[i].Id] = new Category({
    //                id: response[i].Id, title: response[i].Title
    //            });
    //        }
    //    }).error(function (err, status) {
    //        console.error('Could not get categories', err);
    //    });
    //};
    $scope.getChannels = function () {
        console.log("getting channels...");
        $http.get('/api/Channels/').success(function (response) {
            console.log('Channels', response);
            $scope.channels = {};
            for (var i = 0, len = response.length; i < len; ++i) {
                //$scope.categories
                $scope.channels[response[i].Id] = new Channel({
                    id: response[i].Id,
                    description: response[i].Description,
                    link: response[i].Link,
                    title: response[i].Title
                });
            }
        }).error(function (err, status) {
            console.error('Could not get channels', err);
        });
    };

    $scope.openCategory = function (cat) {
        console.log("openCategory");
        if (cat.opened == undefined) {
            rssService.getChannelsFromCategory(cat, function (err, channels) {
                if (!err) {
                    cat.channelsObjects = channels;
                } else {
                    console.error("Could not fetch [%s]", cat.Title);
                }
            });
            cat.opened = true;
        } else {
            cat.opened = !cat.opened;
        }
    };

    $scope.select = function (obj) {
        console.log('select');
        $scope.current.template = $scope.templates.channel;
        $scope.current.data = obj;
    };
    $scope.selectChannel = function (obj) {
        console.log('selectChannel');
        $scope.current.template = $scope.templates.channel;
        $scope.current.data = obj;
        $scope.getItemsFromChannel(obj);
    };

    $scope.selectCategory = function (obj) {

    };

    $scope.logout = function () {
        console.log("Signing out...");
        authService.logOut();
        $state.go('home');
    };

    //$scope.categories = {};
    //$scope.categories["Tech"] = ["korben.info", "nextinpact.com"];
    //$scope.categories["Japan"] = ["sama.animint.fr"];

    //$http.get("/feeds").success(function (data) {
    //    console.log("Loading feeds");
    //    for (var i = 0, len = data.length; i < len; ++i) {
    //        feeds.push(data[i]);
    //    }
    //}).error(function (data) {
    //    console.log("Error loading feeds")
    //});
    authService.fillAuthData();
    if (authService.authentication.isAuth == true) {
        console.log("Authentified");
        $scope.authentified();
    } else {
        console.log("Unauthentified");
    }
});

app.filter('items', function () {
    return function (obj, channels) {
        var items = [];
        if (obj instanceof Channel) {
            console.log('Channel');
            items = obj.items;
        } else if (obj instanceof Category) {
            //Concat all the items arrays
            console.log('Category', obj.channels, channels);
            for (var i = 0, len = obj.channels.length; i < len; ++i) {
                var id = obj.channels[i];
                console.log('Channel', obj.channels[i], id, channels[id]);
                if (channels[id] != undefined) {
                    console.log('Concat');
                    items = items.concat(channels[id].items);
                }
            }
        }
        console.log('items filter', items);
        return items;
    };
});

app.filter('items', function () {
    return function (channels) {
        var items = {};
        //if (obj instanceof Channel) {
        //    console.log('Channel');
        //    items = obj.items;
        //} else if (obj instanceof Category) {
        //Concat all the items arrays
        //console.log('Category', obj.channels, channels);
        for (var c in channels) {
            var chan = channels[c]
            console.log("filter Channel", chan);
            for (var i in chan.itemsObjects) {
                items[i] = chan.itemsObjects[i];
            }
        }
        //for (var i = 0, len = channels.length; i < len; ++i) {
        //        var id = obj.channels[i];
        //        console.log('Channel', obj.channels[i], id, channels[id]);
        //        if (channels[id] != undefined) {
        //            console.log('Concat');
        //            items = items.concat(channels[id].items);
        //        }
        //    }
        //}
        console.log('items filter', items);
        return items;
    };
});
