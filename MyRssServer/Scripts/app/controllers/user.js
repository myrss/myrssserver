﻿var app = angular.module("RSSApp");

app.controller('userCtrl', function ($scope, $http, $modal, $location, authService) {

    $scope.user = {
        name: "Username",
        image: ""
    }

    //$http.get("/feeds").success(function (data) {
    //    console.log("Loading feeds");
    //    for (var i = 0, len = data.length; i < len; ++i) {
    //        feeds.push(data[i]);
    //    }
    //}).error(function (data) {
    //    console.log("Error loading feeds")
    //});

    //$scope.open = function () {
    //    var modalInstance = $modal.open({
    //        templateUrl: 'Templates/Login',
    //        controller: 'ModalInstanceCtrl',
    //        //size: '',
    //        resolve: {
    //            //items: function () {
    //            //    return $scope.credentials;
    //            //}
    //        }
    //    });

    //    modalInstance.result.then(function (credentials) {
    //        //$scope.credentials = credentials;
    //        console.log('Signing in');
    //        authService.login(credentials)
    //            .then(function (response) {
    //                //$location.path('/');
    //                $scope.$emit('authentified');
    //            }, function (err) {
    //                console.error('Login failed', err);
    //                //$scope.message = err.error_description;
    //            });

    //    }, function () {
    //        console.log('Modal dismissed at: ' + new Date());
    //    });
    //};
    $scope.getDetails = function () {
        //TODO: get full user details
        $http.get('/api/Account/UserInfo').success(function (response) {
            console.log('Details', response);
        }).error(function (err, status) {
            console.error('Could not get details', err);
        });
    };
});


//app.controller('ModalInstanceCtrl', function ($scope, $modalInstance) {

//    $scope.credentials = { userName: '', password: '' };

//    $scope.login = function () {
//        $modalInstance.close($scope.credentials);
//    };

//    $scope.cancel = function () {
//        $modalInstance.dismiss('cancel');
//    };
//});