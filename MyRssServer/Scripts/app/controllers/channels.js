﻿function channelAdd($scope, rssService) {
    $scope.channel = new Channel();

    $scope.add = function (channel) {
        console.log("add Channel");
        rssService.addChannel(channel, function (err, response) {
            if (err) {
                console.error("Could not add channel", err, response);
            } else {
                console.log("Channel added", response);
            }
        });
    };
}

function channelEdit($scope, $stateParams, rssService) {
    var channelId = $stateParams.channelId;
    $scope.channel = null;
    rssService.getChannel(channelId, function (err, channel) {
        $scope.channel = channel;
    });
    $scope.add = function (channel) {
        console.log("edit Channel");
        rssService.editChannel(channel, function (err, data) {
            if (!err) {
                console.log("Channel added", data);
                $scope.channel = data;
            } else {
                console.error("Could not edit channel", err, data);
            }
        });
    };
}

function channelItems ($scope, $state, $stateParams, rssService) {
    var channelId = $stateParams.channelId;
    $scope.channel = null;
    $scope.items = null;
    console.log("channel.items", channelId);
    rssService.getChannel(channelId, function (err, channel) {
        if (!err) {
            console.log("Channel", channel);
            $scope.channel = channel;
        } else {
            console.error("Could not get channel [%d]", channelId);
        }
    });
    rssService.getItemsFromChannel(channelId, function (err, items) {
        if (!err) {
            console.log("Items", items);
            $scope.items = items;
            $scope.channel.itemsObjects = items;
        } else {
            console.error("Could not get channel's items [%d]", channelId, err);
        }
    });
    $scope.refresh = function () {
        rssService.refreshChannel(channelId, function (err, status) {
            if (!err) {
                console.log("Channel updated");
            } else {
                console.error("Coul not update channel");
            }
        });
    };
}