﻿function categoryAdd($scope, rssService) {
    //$scope.channel = new Channel();

    //$scope.add = function (channel) {
    //    console.log("add Channel");
    //    rssService.addChannel(channel, function (err, response) {
    //        if (err) {
    //            console.error("Could not add channel", err, response);
    //        } else {
    //            console.log("Channel added", response);
    //        }
    //    });
    //};
}

function categoryItems($scope, $state, $stateParams, rssService) {
    var categoryId = $stateParams.catId;
    console.log("PARAMS", $stateParams, categoryId);
    $scope.category = null;
    $scope.channels = null;
    $scope.items = null;
    rssService.getCategory(categoryId, function (err, category) {
        $scope.category = category;
        console.log("CAT", category);
        rssService.getItemsFromCategory($scope.category, function (items) {
            $scope.items = items;
        });
    });

    //var channelId = $stateParams.channelId;
    //$scope.channel = null;
    //$scope.items = null;
    //console.log("channel.items", channelId);
    //rssService.getChannel(channelId, function (err, channel) {
    //    if (!err) {
    //        console.log("Channel", channel);
    //        $scope.channel = channel;
    //    } else {
    //        console.error("Could not get channel [%d]", channelId);
    //    }
    //});
    //rssService.getItemsFromChannel(channelId, function (err, items) {
    //    if (!err) {
    //        console.log("Items", items);
    //        $scope.items = items;
    //    } else {
    //        console.error("Could not get channel's items [%d]", channelId, err);
    //    }
    //});
    //$scope.refresh = function () {
    //    rssService.refreshChannel(channelId, function (err, status) {
    //        if (!err) {
    //            console.log("Channel updated");
    //        } else {
    //            console.error("Coul not update channel");
    //        }
    //    });
    //};
}