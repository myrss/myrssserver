using MyRssServer.Models;

namespace MyRssServer.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    //internal sealed class Configuration : DbMigrationsConfiguration<MyRssServer.Models.MyRssServerContext>
    //{
    //    public Configuration()
    //    {
    //        AutomaticMigrationsEnabled = false;
    //        ContextKey = "MyRssServer.Models.MyRssServerContext";
    //    }
    internal sealed class Configuration : DbMigrationsConfiguration<MyRssServer.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "MyRssServer.Models.ApplicationDbContext";
        }

        //protected override void Seed(MyRssServer.Models.ApplicationDbContext context)
        //{
        //    //  This method will be called after migrating to the latest version.

        //    //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
        //    //  to avoid creating duplicate seed data. E.g.
        //    //
        //    //    context.People.AddOrUpdate(
        //    //      p => p.FullName,
        //    //      new Person { FullName = "Andrew Peters" },
        //    //      new Person { FullName = "Brice Lambson" },
        //    //      new Person { FullName = "Rowan Miller" }
        //    //    );
        //    //
        //}

        protected override void Seed(MyRssServer.Models.ApplicationDbContext context)
        {
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Categories]");

            var hasher = new PasswordHasher();
            ApplicationUser user = new ApplicationUser()
            {
                UserName = "test@yopmail.com",
                Email = "test@yopmail.com",
                //PasswordHash = hasher.HashPassword("Password1!"),
                Categories = { 
                    new Category()
                    {
                        Title = "Japan",
                        Channels = { 
                            new Channel() {
                                Title = "Aggregator Sama",
                                Link = "sama.animint.fr",
                                LastBuildDate = DateTime.Now
                            },
                        }
                    },
                    new Category()
                    {
                        Title = "Tech",
                        Channels = { 
                            new Channel() {
                                Title = "Next INpact",
                                Link = "www.nextinpact.com",
                                LastBuildDate = DateTime.Now,
                                Items = {
                                    new Item() {
                                        Title = "Star Citizen : 8 millions de dollars de plus en l'espace de deux mois",
                                        Link = "http://www.nextinpact.com/news/91129-star-citizen-8-millions-dollars-plus-en-espace-deux-mois.htm"},                                    new Item() {
                                        Title = "Le Parlement europ�en invite la Commission � scinder Google en deux",
                                        Link = "http://www.nextinpact.com/news/91128-le-parlement-europeen-invite-commission-a-scinder-google-en-deux.htm"}

                                }
                            },
                            new Channel() {
                                Title = "Korben",
                                Link = "korben.info",
                                LastBuildDate = DateTime.Now
                            }
                        }
                    },
                    new Category()
                    {
                        Title = "Cook",
                        Channels = { 
                            new Channel() {
                                Title = "Marmiton",
                                Link = "www.marmiton.org",
                                LastBuildDate = DateTime.Now
                            }
                        }
                    }
                }
            };

            ApplicationUser user2 = new ApplicationUser()
            {
                UserName = "test2@yopmail.com",
                Email = "test2@yopmail.com",
                //PasswordHash = hasher.HashPassword("Password1!"),
                Categories = { 
                    new Category()
                    {
                        Title = "Dev",
                        Channels = { 
                            new Channel() {
                                Title = "MSDN",
                                Link = "http://msdn.microsoft.com/en-us/magazine/rss",
                                LastBuildDate = DateTime.Now
                            },
                        }
                    },
                    new Category()
                    {
                        Title = "Comics",
                        Channels = { 
                            new Channel() {
                                Title = "xkcd",
                                Link = "xkcd.com",
                                LastBuildDate = DateTime.Now
                            },
                            new Channel(){
                                Title = "Buni",
                                Link = "http://www.bunicomic.com/feed/",
                                LastBuildDate = DateTime.Now
                            }
                        }
                    },
                    new Category()
                    {
                        Title = "Tech",
                        Channels = { 
                            new Channel() {
                                Title = "Gizmodo",
                                Link = "http://www.gizmodo.fr/feed",
                                LastBuildDate = DateTime.Now
                            },
                            new Channel() {
                                Title = "TechCrunch",
                                Link = "http://techcrunch.com/feed/",
                                LastBuildDate = DateTime.Now
                            }

                        }
                    }
                }
            };

            var result = UserManager.Create(user, "Password1!");
            var result2 = UserManager.Create(user2, "Password1!");

            //ApplicationUser user2 = new ApplicationUser()
            //{
            //    UserName = "test@yopmail.com",
            //    Email = "test@yopmail.com",
            //    PasswordHash = hasher.HashPassword("Password1!")
            //};
            //var result2 = UserManager.Create(user2, "Password1!");

            //if (result.Succeeded)
            //    System.Diagnostics.Debug.WriteLine("USER CREATED");
            //else
            //    System.Diagnostics.Debug.WriteLine("USER NOT CREATED");
            //context.Users.AddOrUpdate(x => x.Id, {result});
            //context.Categories.AddOrUpdate(x => x.Id,

            //    );
        }
    }
}
