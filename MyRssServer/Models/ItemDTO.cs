﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyRssServer.Models
{
    public class ItemDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }

        //Optionnal
        public string Author { get; set; }
        //public List<Category> Categories { get; set; }
        public string Comments { get; set; }
        //public string Enclosure { get; set; }
        public string GUID { get; set; }
        public DateTime? PubDate { get; set; }

        //Foreign Key
        public int ChannelId { get; set; }

        public bool Read { get; set; }
    }
}