﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyRssServer.Models
{
    public class Channel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        [Required]
        [StringLength(512)]
        [Index("IX_Link", 1, IsUnique = true)]
        public string Link { get; set; }
        public string Description { get; set; }

        //User relation (FK)
        //public int CategoryId { get; set; }

        // Navigation property
        public ICollection<Item> Items { get; set; }
        public ICollection<Category> Categories { get; set; }


        public Channel()
        {
            Items = new List<Item>();
            Categories = new List<Category>();
        }

        //Optionnal
        //public string Language { get; set; }
        //public string Copyright { get; set; }
        //public string ManagingEditor { get; set; }
        //public string WebMaster { get; set; }
        //public string PubDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastBuildDate { get; set; }
        //public List<Category> Category { get; set; }
        //public string Generator { get; set; }
        //public string Docs { get; set; }
        //public string Cloud { get; set; }
        public int TTL { get; set; }
        //public string Image { get; set; }
        //public string Rating { get; set; }
        //public string TextInput { get; set; }
        //public string SkipHours { get; set; }
        //public string SkipDays { get; set; }

        //public Channel(int Id, string Title, string Link, string Description)
        //{
        //    this.Id = Id;
        //    this.Title = Title;
        //    this.Link = Link;
        //    this.Description = Description;
        //}

    }
}