﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyRssServer.Models
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(512)]
        public string Title { get; set; }
        [Required]
        [StringLength(512)]
        [Index("IX_Link", 1, IsUnique = true)]
        public string Link { get; set; }
        public string Description { get; set; }

        //Optionnal
        public string Author { get; set; }
        //public List<Category> Categories { get; set; }
        public string Comments { get; set; }
        //public string Enclosure { get; set; }
        public string GUID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? PubDate { get; set; }


        //Navigation
        public ICollection<Channel> Channels { get; set; }
        public ICollection<ApplicationUser> Users { get; set; }

        public Item()
        {
            Channels = new List<Channel>();
        }

    }
}