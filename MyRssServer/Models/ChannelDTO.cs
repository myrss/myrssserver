﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyRssServer.Models
{
    public class ChannelDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }

        //public ChannelDTO(Channel channel)
        //{
        //    this.Id = channel.Id;
        //    this.Title = channel.Title;
        //    this.Link = channel.Link;
        //    this.Description = channel.Description;
        //}

        //public ChannelDTO(Channel channel, int categoryId) : this(channel)
        //{
        //    this.CategoryId = CategoryId;
        //}
    }
}