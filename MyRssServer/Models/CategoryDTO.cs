﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyRssServer.Models
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        //public string UserId { get; set; }
    }
}