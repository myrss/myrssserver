﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyRssServer.Models
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(256)]
        [Index("IX_TitleUserId", 1, IsUnique = true)]
        public string Title { get; set; }

        //User relation (FK)
        [Index("IX_TitleUserId", 2, IsUnique = true)]
        public string UserId { get; set; }

        // Navigation property
        public ICollection<Channel> Channels { get; set; }
        public ApplicationUser User { get; set; }

        public Category()
        {
            Channels = new List<Channel>();
            //User = new ApplicationUser();
        }
    }
}