﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MyRssServer.Models;
using Microsoft.AspNet.Identity;

namespace MyRssServer.Controllers
{
    [Authorize]
    public class CategoriesController : ApiController
    {
        //private MyRssServerContext db = new MyRssServerContext();
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: api/Categories
        //public IQueryable<Category> GetCategories()
        //{
        //    return db.Categories.Include(c => c.Channels);
        //}
        public IQueryable<CategoryDTO> GetCategories()
        {
            var id = User.Identity.GetUserId();
            IQueryable<Category> data = db.Categories.Where(cat => cat.User.Id == id);
            var categories = from c in data
                             select new CategoryDTO()
                             {
                                 Id = c.Id,
                                 Title = c.Title
                                 //,
                                 //UserId = c.UserId
                             };
            return categories;
        }

        // GET: api/Categories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCategory(int id, Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.Id)
            {
                return BadRequest();
            }

            db.Entry(category).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Categories
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> PostCategory([FromBody]string name)
        {
            var category = new Category();
            category.UserId = User.Identity.GetUserId();
            category.Title = name;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (CategoryExists(category.Title))
            {
                return Conflict();
            }

            db.Categories.Add(category);
            await db.SaveChangesAsync();

            var dto = new CategoryDTO()
            {
                Id = category.Id,
                Title = category.Title,
            };

            return CreatedAtRoute("DefaultApi", new { id = category.Id }, dto);
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> DeleteCategory(int id)
        {
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            db.Categories.Remove(category);
            await db.SaveChangesAsync();

            var dto = new CategoryDTO()
            {
                Id = category.Id,
                Title = category.Title,
            };

            return Ok(dto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.Id == id) > 0;
        }
        private bool CategoryExists(string title)
        {
            var id = User.Identity.GetUserId();
            return db.Categories.Count(e => e.Title == title && e.UserId == id) > 0;
        }
    }
}