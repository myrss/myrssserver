﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MyRssServer.Models;
using Microsoft.AspNet.Identity;

namespace MyRssServer.Controllers
{
    public class ItemsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Items
        //TODO Get by userId
        public IQueryable<ItemDTO> GetItems()
        {
            var id = User.Identity.GetUserId();
            IQueryable<Item> data = db.Items
                .Where(i => i.Channels.Any(ch => ch.Categories.Any(cat => cat.User.Id == id)));
            //    .Any(cat => cat.User.Id == id)
            //    );
            var items = from i in data
                        select new ItemDTO()
                        {
                            Id = i.Id,
                            Title = i.Title,
                            Link = i.Link,
                            Description = i.Description,
                            Read = i.Users.Any(u => u.Id == id)
                            //ChannelId = i.ChannelId
                        };
            return items;
        }
        [Route("~/api/Categories/{categoryId:int}/items")]
        public IQueryable<ItemDTO> GetItemsFromCategory(int categoryId)
        {
            var id = User.Identity.GetUserId();
            IQueryable<Item> data = db.Items
                .Where(i => i.Channels.Any(ch => ch.Categories.Any(cat => cat.User.Id == id && cat.Id == categoryId)));
            var items = from i in data
                        select new ItemDTO()
                        {
                            Id = i.Id,
                            Title = i.Title,
                            Link = i.Link,
                            Description = i.Description,
                            ChannelId = i.Channels.First().Id,
                            Read = i.Users.Any(u => u.Id == id)
                        };
            return items;
        }
        [Route("~/api/Channels/{channelId:int}/items")]
        public IQueryable<ItemDTO> GetItemsFromChannel(int channelId)
        {
            var id = User.Identity.GetUserId();
            IQueryable<Item> data = db.Items
                .Where(i => i.Channels.Any(ch => ch.Id == channelId && ch.Categories.Any(cat => cat.User.Id == id)));
            var items = from i in data
                        select new ItemDTO()
                        {
                            Id = i.Id,
                            Title = i.Title,
                            Link = i.Link,
                            Description = i.Description,
                            ChannelId = i.Channels.FirstOrDefault().Id,
                            Read = i.Users.Any(u => u.Id == id)
                        };
            return items;
        }

        // GET: api/Items/5
        [ResponseType(typeof(Item))]
        public async Task<IHttpActionResult> GetItem(int id)
        {
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        // PUT: api/Items/5/MarkAsRead
        [Route("~/api/Items/{itemsId:int}/MarkAsRead")]
        [HttpGet]
        public ItemDTO ReadItem(int itemsId)
        {
                var item = db.Items.Where(i => i.Id == itemsId).Include(i => i.Users).FirstOrDefault();
                if (item == null)
                    return null;
                var id = User.Identity.GetUserId();
                if (item.Users.All(us => us.Id != User.Identity.GetUserId()))
                    item.Users.Add(db.Users.FirstOrDefault(us => us.Id == id));
               db.SaveChanges();
               return new ItemDTO()
               {
                   Id = item.Id,
                   Title = item.Title,
                   Link = item.Link,
                   Description = item.Description,
                   Read = item.Users.Any(u => u.Id == id)
               };
        }

        [Route("~/api/Items/{itemsId:int}/MarkAsUnRead")]
        [HttpGet]
        public ItemDTO UnReadItem(int itemsId)
        {
            var item = db.Items.Where(i => i.Id == itemsId).Include(i => i.Users).FirstOrDefault();
            if (item == null)
                return null;
            var id = User.Identity.GetUserId();
            if (item.Users.Any(us => us.Id != User.Identity.GetUserId()))
                item.Users.Remove(db.Users.FirstOrDefault(us => us.Id == id));
            db.SaveChanges();
            return new ItemDTO()
            {
                Id = item.Id,
                Title = item.Title,
                Link = item.Link,
                Description = item.Description,
                Read = item.Users.Any(u => u.Id == id)
            };
        }

       
        /*
        // PUT: api/Items/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutItem(int id, Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.Id)
            {
                return BadRequest();
            }

            db.Entry(item).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }*/

        // POST: api/Items
        [ResponseType(typeof(Item))]
        public async Task<IHttpActionResult> PostItem(Item item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Items.Add(item);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = item.Id }, item);
        }

        // DELETE: api/Items/5
        [ResponseType(typeof(Item))]
        public async Task<IHttpActionResult> DeleteItem(int id)
        {
            Item item = await db.Items.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            db.Items.Remove(item);
            await db.SaveChangesAsync();

            return Ok(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ItemExists(int id)
        {
            return db.Items.Count(e => e.Id == id) > 0;
        }
    }
}