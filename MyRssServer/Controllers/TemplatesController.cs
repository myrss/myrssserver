﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyRssServer.Controllers
{
    public class TemplatesController : Controller
    {
        // GET: Templates
        public ActionResult Item()
        {
            return View();
        }
        public ActionResult ItemDetails()
        {
            return View();
        }
        public ActionResult Channel()
        {
            return View();
        }

        public ActionResult Category()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult CategoryCreate()
        {
            return View();
        }

        public ActionResult ChannelCreate()
        {
            return View();
        }

        public ActionResult Home()
        {
            return View();
        }

    }
}