﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Xml;
using MyRssServer.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;

namespace MyRssServer.Controllers
{
    [Authorize]
    public class ChannelsController : ApiController
    {
        //private MyRssServerContext db = new MyRssServerContext();
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Channels
        //public IQueryable<Channel> GetChannels()
        //{
        //    return db.Channels;
        //}
        public IQueryable<ChannelDTO> GetChannels()
        {
            var id = User.Identity.GetUserId();
            IQueryable<Channel> data = db.Channels.Where(ch => ch.Categories
                .Any(cat => cat.User.Id == id)
                );
            IQueryable<ChannelDTO> channels = from c in data
                                              select new ChannelDTO()
                                              {
                                                  Id = c.Id,
                                                  Title = c.Title,
                                                  Link = c.Link,
                                                  Description = c.Description,
                                                  CategoryId = c.Categories.FirstOrDefault().Id
                                              };
            return channels;
        }

        [ResponseType(typeof(Channel))]
        [HttpGet]
        [Route("~/api/Channels/{channelId:int}/refresh")]
        public IHttpActionResult Refresh(int channelId)
        {
            var userId = User.Identity.GetUserId();
            Channel channel = db.Channels
                .Where(ch => ch.Id == channelId && ch.Categories.Any(cat => cat.User.Id == userId))
                .FirstOrDefault();
            if (channel == null)
            {
                return NotFound();
            }
            UpdateOne(channel);
            return Ok(new ChannelDTO()
                      {
                          Id = channel.Id,
                          Title = channel.Title,
                          Link = channel.Link,
                          Description = channel.Description,
                      });
        }

        public int UpdateAll(string magic)
        {
            try
            {
                if (magic != "CoucouRSS")
                    return 300;
                foreach (var channel in db.Channels.ToList())
                {
                    UpdateOne(channel);
                }
                return 200;
            }
            catch (Exception)
            {
                return 500;
            }
        }

        private void UpdateOne(Channel channel)
        {
            try
            {
                db.Entry(channel).Collection(x => x.Items).Load();
                //var currentItems = db.Items.Where(it => it.Channels
                //.Any(chan => chan.Id == channel.Id)).ToList();
                string url = channel.Link;
                XmlReader reader = XmlReader.Create(url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                if (feed != null)
                {
                    System.Diagnostics.Debug.WriteLine("Feed found");
                    foreach (SyndicationItem item in feed.Items)
                    {
                        if (channel.Items.All(it => it.GUID != item.Id))
                        {
                            Item tmp = new Item();
                            var firstOrDefault = item.Authors.FirstOrDefault();
                            if (firstOrDefault != null) tmp.Author = firstOrDefault.Name;
                            tmp.Description = item.Summary.Text;
                            tmp.GUID = item.Id;
                            var syndicationLink = item.Links.FirstOrDefault();
                            if (syndicationLink != null)
                                tmp.Link = syndicationLink.Uri.AbsoluteUri;
                            tmp.PubDate = item.PublishDate.DateTime;
                            tmp.Title = item.Title.Text;
                            channel.Items.Add(tmp);
                        }
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Could not find a feed");
                }
                db.SaveChanges();
            }
            catch (Exception)
            {
            }
        }

        [Route("~/api/Categories/{categoryId:int}/channels")]
        public IQueryable<ChannelDTO> GetByCategory(int categoryId)
        {
            var id = User.Identity.GetUserId();
            //Which is better ?
            // ~110ms
            var data = db.Channels.Where(ch => ch.Categories.Any(cat => cat.Id == categoryId && cat.User.Id == id));
            //~90ms
            //var data = db.Categories.Where(cat => cat.Id == categoryId && cat.User.Id == id).Include(cat => cat.Channels).SelectMany(cat => cat.Channels);
            var channels = from c in data
                           select new ChannelDTO()
                           {
                               Id = c.Id,
                               Title = c.Title,
                               Link = c.Link,
                               Description = c.Description
                           };
            return channels;
        }

        // GET: api/Channels/5
        [ResponseType(typeof(Channel))]
        public async Task<IHttpActionResult> GetChannel(int id)
        {
            Channel channel = await db.Channels.FindAsync(id);
            if (channel == null)
            {
                return NotFound();
            }

            return Ok(channel);
        }

        // PUT: api/Channels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutChannel(int id, Channel channel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != channel.Id)
            {
                return BadRequest();
            }

            db.Entry(channel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChannelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Channels
        [ResponseType(typeof(Channel))]
        //public async Task<IHttpActionResult> PostChannel(Channel channel, List<int> categories)
        public async Task<IHttpActionResult> PostChannel(JObject jsonData)
        {
            try
            {
                dynamic json = jsonData;
                JObject jsonChannel = json.channel;
                JArray jsonCategories = json.categories;
                var userId = User.Identity.GetUserId();

                var channel = jsonChannel.ToObject<Channel>();
                var categories = jsonCategories.ToObject<List<int>>();
                //Channel channel = data.ToObject<Channel>();
                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}
                if (ChannelExists(channel.Link))
                {
                    channel = db.Channels.Where(ch => ch.Link == channel.Link).Include(ch => ch.Categories).FirstOrDefault();
                    db.Entry(channel).State = EntityState.Modified;
                }
                else
                {
                    db.Channels.Add(channel);
                    // Faut pas faire ça, c'est une création...
                    //channel = db.Channels.Where(ch => ch.Link == channel.Link).Include(ch => ch.Categories).FirstOrDefault();
                    //db.Entry(channel).State = EntityState.Modified;
                }
                UpdateAll("CoucouRSS");
                var data = db.Categories.Where(c => c.UserId == userId);
                var cat = from c in data
                          where categories.Contains(c.Id)
                          select c;
                foreach (Category c in cat)
                {
                    channel.Categories.Add(c);
                }

                await db.SaveChangesAsync();

                return CreatedAtRoute("DefaultApi", new { id = channel.Id },
                        new ChannelDTO()
                        {
                            Id = channel.Id,
                            Title = channel.Title
                        }
                    );
            }
            catch (Exception ee)
            {
                System.Diagnostics.Debug.WriteLine(ee.StackTrace);
                return BadRequest("Invalid Parameters");
            }

        }

        // DELETE: api/Channels/5
        [ResponseType(typeof(Channel))]
        public async Task<IHttpActionResult> DeleteChannel(int id, List<int> categories)
        {

            var userId = User.Identity.GetUserId();
            Channel channel = db.Channels
                .Where(ch => ch.Id == id && ch.Categories.Any(cat => cat.User.Id == userId))
                .Include(ch => ch.Categories).FirstOrDefault();
            if (channel == null)
            {
                return NotFound();
            }

            db.Channels.Remove(channel);
            await db.SaveChangesAsync();

            return Ok(channel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ChannelExists(int id)
        {
            return db.Channels.Count(e => e.Id == id) > 0;
        }
        private bool ChannelExists(string link)
        {
            return db.Channels.Any(o => o.Link == link);
        }
    }
}